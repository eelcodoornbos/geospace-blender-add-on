import bpy

def set_f_curves_vector(data_path, frames, xvalues, yvalues, zvalues, object, replace=True):
    # Create an action
    print(object.name, data_path)
    if replace:
        a = bpy.data.actions.new(object.name + " action")
    else:
        a = object.animation_data.action
        
    fc = {}
    for iaxis in [0, 1, 2]:
        # Create the f-curves
        fc[iaxis] = a.fcurves.new(data_path=data_path, index=iaxis, action_group=f"{object.name} {data_path} action group")
        # Add frames
        fc[iaxis].keyframe_points.add(count=len(frames))   
    
    # Populate points    
    fc[0].keyframe_points.foreach_set("co", [x for co in zip(frames, xvalues) for x in co])
    fc[1].keyframe_points.foreach_set("co", [x for co in zip(frames, yvalues) for x in co])
    fc[2].keyframe_points.foreach_set("co", [x for co in zip(frames, zvalues) for x in co])
     
    for iaxis in [0, 1, 2]:
        fc[iaxis].update()
        fc[iaxis].convert_to_samples(frames[0], frames[-1])

    # Animation data

    ad = object.animation_data_create()
    ad.action = a

def set_earth_orientation(date_range):
    from mathutils import Matrix
    matrices = itrs_to_gcrs_matrix(date_range)
    
    # Populate points
    eulers = [Matrix(mat).to_euler() for mat in matrices]
    eulerxs = [euler.x for euler in eulers]
    eulerys = [euler.y for euler in eulers]
    eulerzs = [euler.z for euler in eulers]

    set_f_curves_vector("rotation_euler", 
        range(0, len(eulerxs)), eulerxs, eulerys, eulerzs, 
        bpy.data.objects['Earth empty'])   
###

def set_sun_position(date_range):
    t = Time(date_range)
    sunpos = astropy.coordinates.get_sun(t)    
    sunpos2 = sunpos.transform_to('gcrs')
    sunpos3 = astropy.coordinates.SkyCoord(sunpos2, unit='au', representation_type='cartesian')
    sunposvec = [sunpos3.x.value, sunpos3.y.value, sunpos3.z.value]

    sunposx = [sunpos3[i].x.value for i in range(0, len(sunpos3))]
    sunposy = [sunpos3[i].y.value for i in range(0, len(sunpos3))]
    sunposz = [sunpos3[i].z.value for i in range(0, len(sunpos3))]   
    
    set_f_curves_vector("location",
        range(0, len(sunposx)), sunposx, sunposy, sunposz,
        bpy.data.objects['Sun empty'])
        
        
def set_sat_position(date_range, orbitfile=bpy.path.abspath("//") + 'testorbit_jan2008.pickle',
                     object_name='Daedalus empty'):
    # Read the orbit data
    orbit = pd.read_pickle(orbitfile)

    # Set up the orbit interpolation
    f = interpolate.interp1d(x=pd.to_numeric(orbit.index), y=orbit.values, kind='linear', axis=0)
    orbit_interpolated = pd.DataFrame(index=date_range, data=f(pd.to_numeric(date_range)), columns=orbit.columns)

    # Set positions
    satposx = orbit_interpolated['x'].values / 6378.2
    satposy = orbit_interpolated['y'].values / 6378.2
    satposz = orbit_interpolated['z'].values / 6378.2
    
    set_f_curves_vector("location",
        range(0, len(satposx)), satposx, satposy, satposz,
        bpy.data.objects[object_name])       
    
    # Set Euler attitude
    positions = np.array([orbit_interpolated['x'].values, orbit_interpolated['y'].values, orbit_interpolated['z'].values]).swapaxes(0,1)
    velocities = np.array([orbit_interpolated['vx'].values, orbit_interpolated['vy'].values, orbit_interpolated['vz'].values]).swapaxes(0,1)
    velocities_norm = np.linalg.norm(velocities, axis=1)
    angularmomentum = np.cross(positions, velocities)
    angularmomentum_norm = np.linalg.norm(angularmomentum, axis=1)
    x_unit = velocities / velocities_norm[:,np.newaxis]
    z_unit = angularmomentum / angularmomentum_norm[:,np.newaxis]
    y_unit = np.cross(z_unit, x_unit, axis=1)

    eulerx = []
    eulery = []
    eulerz = []

    rotmatrices = np.column_stack((x_unit, y_unit, z_unit)).reshape(len(orbit_interpolated),3,3)
    for rotmatrix in rotmatrices:
        euler = Matrix(rotmatrix.transpose() ).to_euler()
        eulerx.append(euler.x)
        eulery.append(euler.y)
        eulerz.append(euler.z)
        
    set_f_curves_vector("rotation_euler",
        range(0, len(eulerx)), eulerx, eulery, eulerz,
        bpy.data.objects[object_name], replace=False)

def perifocal_axes_euler(row):
    from mathutils import Matrix
    # Extract position and velocity
    pos = np.array([row['x'], row['y'], row['z']])
    vel = np.array([row['vx'], row['vy'], row['vz']])
        
    # Intermediate quantities
    vel_squared = np.dot(vel, vel)
    rdotv = np.dot(pos, vel)
    mu = 398600.44189 # gravitational parameter of the Earth
    muoverr = mu / np.linalg.norm(pos)

    # Angular momentum and its unit vector
    angularmomentum_vec = np.cross(pos, vel)
    angularmomentum_norm = np.linalg.norm(angularmomentum_vec)
    angularmomentum_unitvec = angularmomentum_vec / angularmomentum_norm

    # Eccentricity and its unit vector
    eccentricity_vec = (np.cross(vel, angularmomentum_vec)/ mu) - (pos/np.linalg.norm(pos))
    eccentricity_norm = np.linalg.norm(eccentricity_vec)
    eccentricity_unitvec = eccentricity_vec / eccentricity_norm
    y_unitvec = np.cross(angularmomentum_unitvec, eccentricity_unitvec)
    mat = np.column_stack((eccentricity_unitvec, y_unitvec, angularmomentum_unitvec))
    
    # Populate points
    euler = Matrix(mat).to_euler()
    
    row['euler_x'] = euler.x
    row['euler_y'] = euler.y
    row['euler_z'] = euler.z
    
    return row

def set_sat_perifocal_axes(date_range,  orbitfile=bpy.path.abspath("//") + 'testorbit_jan2008.pickle'):
    # Read the orbit data
    orbit = pd.read_pickle(orbitfile)

    # Set up the orbit interpolation
    f = interpolate.interp1d(x=pd.to_numeric(orbit.index), y=orbit.values, kind='linear', axis=0)
    orbit_interpolated = pd.DataFrame(index=date_range, data=f(pd.to_numeric(date_range)), columns=orbit.columns)
    print(orbit_interpolated)
    orbit_eulers = orbit_interpolated.apply(perifocal_axes_euler, axis=1)
    
    eulerx = orbit_eulers['euler_x'].values
    eulery = orbit_eulers['euler_y'].values
    eulerz = orbit_eulers['euler_z'].values
    
    set_f_curves_vector("rotation_euler",
        range(0, len(eulerx)), eulerx, eulery, eulerz,
        bpy.data.objects['Perifocal axes empty'])       