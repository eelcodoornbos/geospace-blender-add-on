import bpy
import numpy as np
import pandas as pd
from dateutil.parser._parser import ParserError
import os
import pathlib

valid_datetime = False
datetime_index = None

class VIEW3D_PT_satelliteorbits(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'Geospace'
    bl_label = 'Satellite orbits'

    def draw(self, context):
        col = self.layout.column(align=True)
        col.prop(context.scene, 'starttime')
        col.label(text='Time between frames:')
        row = self.layout.row(align=True)
        row.prop(context.scene, 'frequency_multiplier')
        row.prop(context.scene, 'frequency')

        col = self.layout.column(align=True)
        #col.prop(context.scene, 'frame_end')
        if valid_datetime:
            col.label(text=f'Frame: {context.scene.frame_current}')
            col.label(text=f'Date/time: {datetime_index[context.scene.frame_current]}')
        else:
            col.label(text=f'Error parsing {context.scene.starttime}')            

# def on_update_timeseries(scene, context):
#     global datetime_index, valid_datetime
#     valid_datetime = True    
#     try:
#         starttime = pd.to_datetime(scene.starttime)
#         datetime_index = pd.date_range(start=starttime, freq=f'{scene.frequency_multiplier}{scene.frequency}', periods=scene.frame_end)
#     except ParserError:
#         valid_datetime = False
#         datetime_index = None

blender_classes = [
	VIEW3D_PT_satelliteorbits,
]

def register():
	bpy.types.


    bpy.types.Scene.starttime = bpy.props.StringProperty(
        name='Start date/time', 
        default=pd.Timestamp.now().round('D').isoformat(),
        update=on_update_timeseries)

    bpy.types.Scene.frequency = bpy.props.EnumProperty(
        name='unit',
        items=[('S', 'seconds', 'seconds'), ('min', 'minutes', 'minutes'), ('H', 'hours', 'hours'), ('D', 'days', 'days'), ('Y', 'years', 'years')],
        update=on_update_timeseries
        )

    bpy.types.Scene.frequency_multiplier = bpy.props.IntProperty(
        name='Freq',
        default=1,
        min=1,
        max=10000,
        update=on_update_timeseries)

    for blender_class in blender_classes:
    	bpy.utils.register_class(blender_class)

def unregister():
	for blender_class in blender_classes:
		bpy.utils.unregister_class(blender_class)
 
    del bpy.types.Scene.starttime
    del bpy.types.Scene.frequency
    del bpy.types.Scene.frequency_multiplier

