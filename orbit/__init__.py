import bpy
import mathutils
import numpy as np
import pandas as pd
from . import kepler_orbit
from .. import timeseries

earthradius = 6378.1363

def setup_kepler(perigeeheight, apogeeheight, inclination, argumentofperigee, raan, meananomaly):
    perigeeradius = earthradius + perigeeheight
    apogeeradius  = earthradius + apogeeheight
    kepler = {
        "semimajoraxis": (perigeeradius + apogeeradius) / 2,
        "eccentricity": (apogeeradius - perigeeradius) / (apogeeradius + perigeeradius),
        "inclination": np.radians(inclination),
        "argumentofperigee": np.radians(argumentofperigee),
        "raan": np.radians(raan),
        "initial_mean_anomaly": np.radians(meananomaly),
        "mu": 398600.4415,
        "re": 6378.1363,
        "j2": 1082.6357e-6
    }
    return kepler

class AddOrbitCurve(bpy.types.Operator):
    """Add a sphere segment mesh"""
    bl_idname = "curve.orbit_add"
    bl_label = "Orbit curve"
    bl_options = {'REGISTER', 'UNDO'}

    hp: bpy.props.FloatProperty(
        name="Perigee height",
        description="Perigee height",
        min=0, max=40000,
        default=140,
        step=10*100,
    )
    ha: bpy.props.FloatProperty(
        name="Apogee height",
        description="Apogee height",
        min=0, max=40000,
        default=1400,
        step=10*100,        
    )
    inclination: bpy.props.FloatProperty(
        name="Inclination",
        description="Inclination",
        min=0, max=180.0,
        default=85.0,
        step=100,        
    )
    argper: bpy.props.FloatProperty(
        name="Argument of perigee",
        description="Argument of perigee",
        min=0, max=360.0,
        default=0,
        step=100,        
    )
    raan: bpy.props.FloatProperty(
        name="Right ascension of the ascending node",
        description="Right ascension of the ascending node",
        min=0.0, max=360.0,
        default=0.0,
        step=100,        
    )        
    meananomaly: bpy.props.FloatProperty(
        name="Mean anomaly",
        description="Mean anomaly",
        min=0, max=360,
        default=0,
        step=100,        
    )
    stepsperrev: bpy.props.IntProperty(
        name="Steps per revolution",
        description="Steps per revolution",
        min=4, max=300,
        default=60,
    )

    revolutions: bpy.props.FloatProperty(
        name="Revolutions",
        description="Revolution",
        min=0.05, max=300,
        default=1,
    )

    bezierfactor: bpy.props.FloatProperty(
        name="Bezier factor",
        description="Bezier factor",
        min=0.05, max=10,
        default=2.71828,
    )


    def execute(self, context):
        object_name = "Orbit curve"

        # Compute the orbit data
        kepler_elements = setup_kepler(
            self.hp,
            self.ha,
            self.inclination,
            self.argper,
            self.raan,
            self.meananomaly
            )
        tstart = timeseries.datetime_index[0]
        orbitalperiod = kepler_orbit.unperturbed_orbitalperiod(kepler_elements)
        tend = tstart + pd.to_timedelta(self.revolutions * orbitalperiod, unit='s')
        periods = int(self.revolutions * self.stepsperrev)
        time_index = pd.date_range(start=tstart, end=tend, periods=periods)

        times_sec = (time_index - time_index[0])/pd.to_timedelta(1,'s') # Convert the pandas timing to an array containing second offsets
        orbit_array = kepler_orbit.simulate_orbit(kepler_elements, times_sec)
        orbitdataframe = pd.DataFrame(data=orbit_array, index=time_index, columns = ['x', 'y', 'z', 'vx', 'vy', 'vz'])        

        xx = orbitdataframe['x'].values / 6378.2
        yy = orbitdataframe['y'].values / 6378.2
        zz = orbitdataframe['z'].values / 6378.2
        vx = orbitdataframe['vx'].values
        vy = orbitdataframe['vy'].values
        vz = orbitdataframe['vz'].values

        # Create the orbit curve data
        curveData = bpy.data.curves.new('orbit curve', type='CURVE')
        curveData.dimensions = '3D'
        curveData.resolution_u = 2

        bezier = curveData.splines.new('BEZIER')
        #if self.revolutions == 1.0:
            #bezier.use_cyclic_u = True
        bezier.bezier_points.add(periods-1)
        points = curveData.splines[0].bezier_points
        for i in range(len(xx)):
            points[i].handle_left_type = 'FREE'
            points[i].handle_right_type = 'FREE'
            points[i].co = (xx[i], yy[i], zz[i])
            vel = mathutils.Vector((vx[i], vy[i], vz[i]))
            points[i].handle_left = points[i].co - vel * (self.bezierfactor / self.stepsperrev) * (self.hp + self.ha + 2*earthradius)/(earthradius)
            points[i].handle_right = points[i].co + vel * (self.bezierfactor / self.stepsperrev) * (self.hp + self.ha + 2*earthradius)/(earthradius)


        #points[0].handle_left_type = 'VECTOR'
        #points[len(xx)-1].handle_right_type = 'VECTOR'

        # Create the orbit object
        obj = bpy.data.objects.new(object_name, curveData)
        obj.data.bevel_depth = 0.01
        #obj.data.taper_object = bpy.data.objects["Orbit taper"]
        #obj.data.bevel_object = bpy.data.objects["Orbit cap"]
        obj.data.use_fill_caps = True
        context.collection.objects.link(obj)
        # Set active object
        return {'FINISHED'}


blender_classes = [
    AddOrbitCurve,
]

blender_menu_items = {
	AddOrbitCurve,
}

def register():
    for blender_class in blender_classes:
        bpy.utils.register_class(blender_class)
#    bpy.types.VIEW3D_MT_mesh_add.append(menu_func_spheresegment)
#    bpy.types.VIEW3D_MT_mesh_add.append(menu_func_mltspheresegment)

def unregister():
    for blender_class in blender_classes:
        bpy.utils.unregister_class(blender_class) 
#    bpy.types.VIEW3D_MT_mesh_add.remove(menu_func_spheresegment)
#    bpy.types.VIEW3D_MT_mesh_add.remove(menu_func_mltspheresegment)

