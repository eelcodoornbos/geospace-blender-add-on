import bpy
import numpy as np
import pandas as pd
from dateutil.parser._parser import ParserError
import os
import pathlib

valid_datetime = False
datetime_index = None
test = "test"

class VIEW3D_PT_timeseries(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'Geospace'
    bl_label = 'Set up UTC time per frame'

    def draw(self, context):
        col = self.layout.column(align=True)
        col.prop(context.scene, 'starttime')
        col.label(text='Time between frames:')
        row = self.layout.row(align=True)
        row.prop(context.scene, 'frequency_multiplier')
        row.prop(context.scene, 'frequency')

        col = self.layout.column(align=True)
        #col.prop(context.scene, 'frame_end')
        if valid_datetime:
            col.label(text=f'Frame: {context.scene.frame_current}')
            col.label(text=f'Date/time: {datetime_index[context.scene.frame_current]}')
        else:
            col.label(text=f'Error parsing {context.scene.starttime}')            

def on_update_timeseries(scene, context):
    global datetime_index, valid_datetime
    valid_datetime = True
    try:
        starttime = pd.to_datetime(scene.starttime)
        datetime_index = pd.date_range(start=starttime, freq=f'{scene.frequency_multiplier}{scene.frequency}', periods=scene.frame_end+1)
    except ParserError:
        valid_datetime = False
        datetime_index = None

def register():
    global datetime_index, valid_datetime
    # Set up some nice details
    default_frequency_multiplier = 5
    default_frequency = 'min'
    default_starttime = pd.Timestamp.now().round('D').isoformat()
    default_periods = 251 # TODO: Find out a way to access this from the scene here

    bpy.types.Scene.starttime = bpy.props.StringProperty(
        name='Start date/time', 
        default=default_starttime,
        update=on_update_timeseries)

    bpy.types.Scene.frequency = bpy.props.EnumProperty(
        name='unit',
        items=[('S', 'seconds', 'seconds'), ('min', 'minutes', 'minutes'), ('H', 'hours', 'hours'), ('D', 'days', 'days'), ('Y', 'years', 'years')],
        update=on_update_timeseries,
        default=default_frequency,
        )

    bpy.types.Scene.frequency_multiplier = bpy.props.IntProperty(
        name='Freq',
        default=default_frequency_multiplier,
        min=1,
        max=100000,
        soft_min=1,
        soft_max=1000,
        update=on_update_timeseries)

    # TODO: Find out a way to access the number of frames here.
    datetime_index = pd.date_range(start=default_starttime, freq=f'{default_frequency_multiplier}{default_frequency}', periods=default_periods) 
    valid_datetime = True

    bpy.utils.register_class(VIEW3D_PT_timeseries)

def unregister():

    bpy.utils.unregister_class(VIEW3D_PT_timeseries)
    del bpy.types.Scene.starttime
    del bpy.types.Scene.frequency
    del bpy.types.Scene.frequency_multiplier

