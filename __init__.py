from . import timeseries
from . import sphere_segments
from . import orbit
import bpy
import importlib
importlib.reload(timeseries)
importlib.reload(sphere_segments)
importlib.reload(orbit)

bl_info = {
    "name": "Tools to help add, manipulate and animate geospace-related objects, such as the Earth, Sun, magnetic field lines, GCM model output and satellites.",
    "blender": (2, 80, 0),
    "category": "Object",
    "author": "Eelco Doornbos <eelco.doornbos@knmi.nl>",
}

class GeospaceMeshCustomMenuAdd(bpy.types.Menu):
    bl_idname = "INFO_MT_mesh_custom_menu_add"
    bl_label = "Geospace"

    def draw(self, context):
        self.layout.operator_context = 'INVOKE_REGION_WIN'
        self.layout.operator("mesh.sphere_segment_add")
        self.layout.operator("mesh.mlt_segment_add")

#        self.layout.menu("INFO_MT_mesh_decoration_add", text="Decoration props", icon="GROUP")


def GeospaceMenu_func(self, context):
    self.layout.menu("INFO_MT_mesh_custom_menu_add", icon="PLUGIN")


def register():
    bpy.utils.register_class(GeospaceMeshCustomMenuAdd)
    timeseries.register()
    sphere_segments.register()
    orbit.register()
    bpy.types.VIEW3D_MT_mesh_add.append(GeospaceMenu_func)

def unregister():
    timeseries.unregister()
    sphere_segments.unregister()
    orbit.unregister()
