import numpy as np
import apexpy
import astropy

def lt_to_lon(lt0, lt1, coordinatetype, t):
    A = apexpy.Apex(t)
    if lt0 == 0.0 and lt1 == 24.0:
        lon0 = 0.0
        lon1 = 360.0 # Force closed ring geometry
    else:
        if coordinatetype == 'apex' or coordinatetype == 'quasi-dipole':
            lon0 = A.mlt2mlon(lt0, t) # Convert MLT to magnetic longitude
            lon1 = A.mlt2mlon(lt1, t)
        else:
            ssglat, ssglon = apexpy.helpers.subsol(t)
            lon0 = (15*lt0 - 180 + ssglon + 360) % 360 # Convert LST to longitude
            lon1 = (15*lt1 - 180 + ssglon + 360) % 360

    # Ensure order of lon0 and lon1
    if lon1 < lon0:
        lon1 += 360.0

    return lon0, lon1

def sphere_segment(lat0, lat1, lon0, lon1, height0, height1, numlat, numlon, numheight, coordinatetype, datetime):
    """Wrapper function for the open and closed ring geometry functions"""
    if lon1 - lon0 == 360.0:
        return sphere_segment_zonalring(lat0, lat1, height0, height1, numlat, numlon, numheight, coordinatetype, datetime)
    else:
        return sphere_segment_open(lat0, lat1, lon0, lon1, height0, height1, numlat, numlon, numheight, coordinatetype, datetime)


def grids_to_vertices(lons, lats, heights, earthradius, coordinatetype, A):
    """Convert grids of lat/lon/height coordinates to Cartesian coordinates of vertices""" 
    # Normalize for Earth radius
    radii = (earthradius + heights)/earthradius
    
    # Create grid meshes, so that coordinate components can be combined
    longrid, latgrid, radiusgrid = np.meshgrid(lons, lats, radii)
    heightgrid = radiusgrid*earthradius - earthradius

    # Optionally convert to geomagnetic coordinates
    if coordinatetype == "quasi-dipole":
        latgrid, longrid, error = A.qd2geo(latgrid, longrid, heightgrid)
    elif coordinatetype == "apex":
        latgrid, longrid, error = A.apex2geo(latgrid, longrid, heightgrid)

    # Feed to astropy coordinate transformation routine
    lonsrad = np.radians(longrid.flatten())
    latsrad = np.radians(latgrid.flatten())
    cartesians = astropy.coordinates.spherical_to_cartesian(radiusgrid.flatten(), latsrad, lonsrad)

    return np.array(cartesians).swapaxes(0,1).tolist()


def sphere_segment_open(lat0, lat1, lon0, lon1, height0, height1, numlat, numlon, numheight, coordinatetype, datetime):
    """
    This function takes inputs and returns vertex and face arrays.
    no actual mesh data creation is done here.
    """
    earthradius = 6378.2 # remove later

    lats = np.linspace(lat0, lat1, numlat)
    lons = np.linspace(lon0, lon1, numlon)
    heights = np.linspace(height0, height1, numheight)    

    verts = []
    faces = []
    
    # Set up magnetic coordinate conversion
    A = apexpy.Apex(date=datetime)
        
    # bottom panel vertices
    new_verts = grids_to_vertices(lons, lats, np.array([height0]), earthradius, coordinatetype, A)
    verts.extend( new_verts )
    offsettop = numlat*numlon

    # top panel vertices
    new_verts = grids_to_vertices(lons, lats, np.array([height1]), earthradius, coordinatetype, A)    
    verts.extend( new_verts )
    offsetwest = offsettop + numlat * numlon

    # west panel vertices
    new_verts = grids_to_vertices(np.array([lon0]), lats, heights[1:-1], earthradius, coordinatetype, A)
    verts.extend( new_verts )
    offsetsouth = offsetwest + (numheight-2) * numlat    

    # south panel vertices
    new_verts = grids_to_vertices(lons[1:-1], np.array([lat0]), heights[1:-1], earthradius, coordinatetype, A)
    verts.extend( new_verts )
    offseteast = offsetsouth + (numheight-2) * (numlon-2)

    # east panel vertices
    new_verts = grids_to_vertices(np.array([lon1]), lats, heights[1:-1], earthradius, coordinatetype, A)
    verts.extend( new_verts )
    offsetnorth = offseteast + (numlat)*(numheight-2)

    # north panel vertices
    new_verts = grids_to_vertices(lons[1:-1], np.array([lat1]), heights[1:-1], earthradius, coordinatetype, A)
    verts.extend( new_verts )

    # bottom panel faces
    for i in range((numlat-1)*numlon):
        if (i+1)%numlon != 0:
            newface = [i+numlon, i+numlon+1, i+1, i]
            faces.append(newface)

    # top panel faces
    for i in range((numlat-1)*numlon):
        if (i+1)%numlon != 0:
            newface = [i+offsettop, i+offsettop+1, i+offsettop+numlon+1, i+offsettop+numlon]
            faces.append(newface)

    # connecting bottom and west panel faces
    for i in range(numlat -1):
        newface = [offsetwest + (i) * (numheight-2),
                   offsetwest + (i+1) * (numheight-2), 
                   (i+1)*numlon, 
                   i*numlon]
        faces.append(newface)
        
    # middle section of west panel faces
    for i in range((numlat-1)*(numheight-2)):
        if (i+1)%(numheight-2) != 0:
            newface = [offsetwest + i, 
                       offsetwest + i+1, 
                       offsetwest + i + numheight-2 + 1, 
                       offsetwest + i + numheight-2]
            faces.append(newface)

    # connecting top and west panel faces
    for i in range(numlat - 1):
        newface = [numlat * numlon + i * numlon,
                   numlat * numlon + (i+1) * numlon,
                   offsetwest + (i+1)*(numheight-2) + numheight -3,
                   offsetwest + i*(numheight-2) + numheight -3]
        faces.append(newface)
    
    # south panel bottom left face
    newface = [0, 1, offsetsouth, offsetwest]
    faces.append(newface)
    
    # south panel top left face
    newface = [offsetwest + numheight-3, offsetsouth + numheight-3, offsettop+1, offsettop]
    faces.append(newface)
    
    # south panel bottom connecting faces
    for i in range(numlon-2):
        newface = [i+1, i+2, offsetsouth+(i+1)*(numheight-2), offsetsouth+i*(numheight-2)]
        faces.append(newface)
    
    # south panel west row connecting faces
    for i in range(numheight-3):
        newface = [offsetwest+i, offsetsouth+i, offsetsouth+i+1, offsetwest+i+1]
        faces.append(newface)
    
    # south panel top row connecting faces
    for i in range(numlon-2):
        newface = [offsetsouth + (i+1)*(numheight-2) - 1,
                   offsetsouth + (i+2)*(numheight-2) - 1,
                   offsettop + i+2,
                   offsettop + i+1]
        faces.append(newface)
        
    # south panel remaining faces        
    for i in range((numheight-2)*(numlon-2)):
        if (i+1)%(numheight-2) != 0:
            newface = [offsetsouth + i + (numheight-2),
                       offsetsouth + i + (numheight-2) + 1,
                       offsetsouth + i+1,
                       offsetsouth + i]                 
            faces.append(newface)

    # east panel bottom row faces
    for i in range(numlat-1):
        newface = [(i+1)*(numlon-1) + i, 
                   (i+2)*(numlon-1) + i + 1,
                   offseteast+(i+1)*(numheight-2),
                   offseteast+(i)*(numheight-2)]
        faces.append(newface)
    
    # east panel top row faces
    for i in range(numlat-1):
        newface = [offseteast + (i) * (numheight-2) + numheight-3,
                   offseteast + (i+1) * (numheight-2) + numheight-3,
                   offsettop + numlon - 1 + (i+1)*(numlon),
                   offsettop + numlon - 1 + (i)*(numlon)]

        faces.append(newface)
        
    # east panel middle faces
    for i in range((numheight-2)*(numlat-1)):
        if (i+1)%(numheight-2) !=0:
            newface = [offseteast + i + (numheight-2),
                       offseteast + i + (numheight-2) + 1,
                       offseteast + i+1,                                  
                       offseteast + i]
            faces.append(newface)

    # north panel middle row faces
    for i in range((numheight-2)*(numlon-3)):
        if (i+1)%(numheight-2) !=0:
            newface = [offsetnorth + i,
                       offsetnorth + i + 1,
                       offsetnorth + i + (numheight-2) + 1,                   
                       offsetnorth + i + (numheight-2)]
            faces.append(newface)

    # north panel corner south west
    newface = [offsetwest + (numheight-2) * (numlat - 1), 
               offsetnorth, 
               numlon*(numlat-1)+1,
               numlon*(numlat-1)]
    faces.append(newface)

    # north panel corner south east
    newface = [offsetnorth + (numheight-2)*(numlon-3),
               offseteast + (numlat-1)*(numheight-2),
               numlon*numlat-1, 
               numlon*numlat-2]
    faces.append(newface)

    # north panel corner north east
    newface = [offsetnorth - 1, 
               offsetnorth + (numlon-2)*(numheight-2)-1,
               2*numlon*numlat-2,               
               2*numlon*numlat-1]
    faces.append(newface)

    # north panel corner north west
    newface = [numlon * numlat + numlon * (numlat -1),
               numlon * numlat + numlon * (numlat - 1) + 1,
               offsetnorth + numheight - 3,
               offsetwest + (numheight - 2) * (numlat) - 1]
    faces.append(newface)

    # bottom row of north face
    for i in range(numlon -3):
        newface = [offsetnorth + i * (numheight - 2),
                   offsetnorth + (i+1) * (numheight-2),
                   numlon * (numlat-1) + 2 + i,
                   numlon * (numlat-1) + 1 + i]
        faces.append(newface)
    
    # top row of north face
    for i in range(numlon -3):
        newface = [offsettop + numlon*(numlat-1)+(i+1),
                   offsettop + numlon*(numlat-1)+(i+2),
                   offsetnorth + (numheight-2)*(i+2)-1,
                   offsetnorth + (numheight-2)*(i+1)-1]
        faces.append(newface)
        
    # west column of north face
    for i in range(numheight-3):
        newface = [offsetwest + (numheight-2) * (numlat-1) + i,
                   offsetwest + (numheight-2) * (numlat-1) + i + 1,
                   offsetnorth + i + 1,
                   offsetnorth + i]
        faces.append(newface)
              
    # east collumn of north face
    for i in range(numheight-3):
        newface = [offsetnorth + (numlon-3)*(numheight-2) + i,
                   offsetnorth + (numlon-3)*(numheight-2) + i + 1,
                   offseteast + (numlat-1)*(numheight-2) + i + 1,
                   offseteast + (numlat-1)*(numheight-2) + i]
        faces.append(newface)

    return verts, faces



def sphere_segment_zonalring(lat0, lat1, height0, height1, numlat, numlon, numheight, coordinatetype, datetime):
    """
    This function takes inputs and returns vertex and face arrays.
    no actual mesh data creation is done here.
    """
    earthradius = 6378.2 # remove later

    lats = np.linspace(lat0, lat1, numlat)
    lons = np.linspace(0, 360, numlon, endpoint=False)
    heights = np.linspace(height0, height1, numheight)    

    verts = []
    faces = []

    # Setup magnetic coordinate conversion    
    A = apexpy.Apex(date=datetime)
        
    # bottom panel vertices
    new_verts = grids_to_vertices(lons, lats, np.array([height0]), earthradius, coordinatetype, A)
    verts.extend( new_verts )
    offsettop = numlat*numlon

    # top panel vertices
    new_verts = grids_to_vertices(lons, lats, np.array([height1]), earthradius, coordinatetype, A)    
    verts.extend( new_verts )
    offsetsouth = offsettop + numlat * numlon

    # south panel vertices
    new_verts = grids_to_vertices(lons, np.array([lat0]), heights[1:-1], earthradius, coordinatetype, A)
    verts.extend( new_verts )
    offsetnorth = offsetsouth + (numheight-2) * numlon

    # north panel vertices
    new_verts = grids_to_vertices(lons, np.array([lat1]), heights[1:-1], earthradius, coordinatetype, A)
    verts.extend( new_verts )

    # bottom panel faces
    for i in range((numlat-1)*numlon):
        if (i+1)%numlon != 0:
            newface = [i+numlon, i+numlon+1, i+1, i]
            faces.append(newface)

    # bottom panel close ring
    for i in range(numlat-1):
        newface = [i*numlon, (i+1)*numlon-1, (i+2)*numlon-1, (i+1)*numlon]
        faces.append(newface)

    # top panel faces
    for i in range((numlat-1)*numlon):
        if (i+1)%numlon != 0:
            newface = [i+offsettop, i+offsettop+1, i+offsettop+numlon+1, i+offsettop+numlon]
            faces.append(newface)

    # top panel close ring
    for i in range(numlat-1):
        newface = [offsettop+i*numlon, offsettop+(i+1)*numlon, offsettop+(i+2)*numlon-1, offsettop+(i+1)*numlon-1]
        faces.append(newface)
        
        
    # south panel bottom connecting faces
    for i in range(numlon-1):
        newface = [i, i+1, offsetsouth+(i+1)*(numheight-2), offsetsouth+i*(numheight-2)]
        faces.append(newface)

    # south panel bottom row complete ring
    newface = [offsetsouth,                
               offsetsouth + (numlon-1)*(numheight-2), 
               numlon-1,                
               0]
    faces.append(newface)
    
    # south panel top row connecting faces
    for i in range(numlon-1):
        newface = [offsetsouth + (i+1)*(numheight-2) - 1,
                   offsetsouth + (i+2)*(numheight-2) - 1,
                   offsettop + i+1,
                   offsettop + i]
        faces.append(newface)

    # south panel top row complete ring
    newface = [offsetsouth + numheight - 3, 
               offsettop,
               offsettop + numlon -1,
               offsetsouth + (numheight-2)*(numlon) - 1]
    faces.append(newface)
    
    # south panel remaining faces        
    for i in range((numheight-2)*(numlon-1)):
        if (i+1)%(numheight-2) != 0:
            newface = [offsetsouth + i + (numheight-2),
                       offsetsouth + i + (numheight-2) + 1,
                       offsetsouth + i+1,
                       offsetsouth + i]
            faces.append(newface)

    # south panel close ring
    for i in range(numheight-3):
        newface = [offsetsouth + i, offsetsouth + i + 1, offsetsouth + (numheight-2)*(numlon-1)+i+1, offsetsouth + (numheight-2)*(numlon-1)+i]
        faces.append(newface)


    # north panel middle row faces
    for i in range((numheight-2)*(numlon-1)):
        if (i+1)%(numheight-2) !=0:
            newface = [offsetnorth + i,
                       offsetnorth + i + 1,
                       offsetnorth + i + (numheight-2) + 1,                   
                       offsetnorth + i + (numheight-2)]
            faces.append(newface)

    # north panel middle row complete ring
    for i in range(numheight-3):
        newface = [offsetnorth + (numheight-2)*(numlon-1) + i,
                   offsetnorth + (numheight-2)*(numlon-1) + i + 1,
                   offsetnorth + i + 1,                   
                   offsetnorth + i]
        faces.append(newface)

    # bottom row of north face
    for i in range(numlon - 1):
        newface = [offsetnorth + i * (numheight - 2),
                   offsetnorth + (i+1) * (numheight-2),
                   numlon * (numlat-1) + 1 + i,
                   numlon * (numlat-1) + i]                  
        faces.append(newface)

    # bottom row of north face, complete ring
    newface = [(numlat-1) * numlon,
               numlat * numlon - 1,
               offsetnorth + (numlon-1)*(numheight-2),
               offsetnorth]
    faces.append(newface)
    
    # top row of north face
    for i in range(numlon - 1):
        newface = [offsettop + numlon*(numlat-1)+i,
                   offsettop + numlon*(numlat-1)+i+1,
                   offsetnorth + (numheight-2)*(i+2)-1,
                   offsetnorth + (numheight-2)*(i+1)-1]
        faces.append(newface)
    
    # top row of north face, complete ring
    newface = [offsetnorth + (numheight - 2) * numlon - 1,
               offsettop + (numlat) * numlon - 1,
               offsettop + (numlat-1) * numlon,
               offsetnorth + numheight - 3]
    faces.append(newface)

    return verts, faces