import bpy
import bmesh
from bpy_extras.object_utils import AddObjectHelper
import numpy as np
import astropy.coordinates
import pandas as pd
import apexpy
from .sphere_segment_geometry import sphere_segment, lt_to_lon
from .. import timeseries

bl_info = {
    "name": "Latitude, longitude, local time and height segments of spheres",
    "blender": (2, 80, 0),
    "category": "Mesh",
    "author": "Eelco Doornbos <eelco.doornbos@knmi.nl>",
}


class AddSphereSegment(bpy.types.Operator):
    """Add a sphere segment mesh"""
    bl_idname = "mesh.sphere_segment_add"
    bl_label = "Sphere Segment"
    bl_options = {'REGISTER', 'UNDO'}

    lat0: bpy.props.FloatProperty(
        name="Lat0",
        description="Start latitude",
        min=-90, max=90.0,
        default=-90.0,
    )
    lat1: bpy.props.FloatProperty(
        name="Lat1",
        description="End latitude",
        min=-90, max=90.0,
        default=90.0,
    )
    lon0: bpy.props.FloatProperty(
        name="Lon0",
        description="Start longitude",
        min=-720, max=360.0,
        default=0.0,
    )
    lon1: bpy.props.FloatProperty(
        name="Lon1",
        description="End longitude",
        min=-360, max=720.0,
        default=360.0,
    )    
    height0: bpy.props.FloatProperty(
        name="Height0",
        description="Start height",
        min=0, max=10000,
        default=0,
    )
    height1: bpy.props.FloatProperty(
        name="Height1",
        description="End height",
        min=0, max=10000,
        default=500,    
    )
    numlons: bpy.props.IntProperty(
        name="NumLons",
        description="Number of longitude steps",
        min=3, max=255,
        default=64,
    )
    numlats: bpy.props.IntProperty(
        name="NumLats",
        description="Number of latitude steps",
        min=3, max=255,
        default=32,
    )   
    numheights: bpy.props.IntProperty(
        name="NumHeights",
        description="Number of height steps",
        min=3, max=255,
        default=3,
    )
    year: bpy.props.FloatProperty(
        name="GeomagYear",
        description="Year for geomagnetic coordinate conversion",
        min=1950, max=2030,
        default=2020,
    )

    coordinate_options = (
        ('geographic', 'Geographic', 'Geographic coordinates'),
        ('quasi-dipole', 'Quasi-dipole', 'Quasi-dipole geomagnetic coordinates'),
        ('apex', 'Apex', 'Apex geomagnetic coordinates')
    )

    coordinatetype: bpy.props.EnumProperty(
        name="Coordinate type",
        items=coordinate_options,
        default="geographic",
    )

    # generic transform props
    align_items = (
        ('WORLD', "World", "Align the new object to the world"),
        ('VIEW', "View", "Align the new object to the view"),
        ('CURSOR', "3D Cursor", "Use the 3D cursor orientation for the new object")
    )
    align: bpy.props.EnumProperty(
        name="Align",
        items=align_items,
        default='WORLD',
        update=AddObjectHelper.align_update_callback,
    )
    location: bpy.props.FloatVectorProperty(
        name="Location",
        subtype='TRANSLATION',
    )
    rotation: bpy.props.FloatVectorProperty(
        name="Rotation",
        subtype='EULER',
    )

    def execute(self, context):

        t = timeseries.datetime_index[0].to_pydatetime()
        verts_loc, faces = sphere_segment(
            self.lat0, 
            self.lat1, 
            self.lon0, 
            self.lon1, 
            self.height0, 
            self.height1,
            self.numlats,
            self.numlons,
            self.numheights,
            self.coordinatetype,
            t
        )

        mesh = bpy.data.meshes.new("Sphere segment")

        bm = bmesh.new()

        for v_co in verts_loc:
            bm.verts.new(v_co)

        bm.verts.ensure_lookup_table()
        for f_idx in faces:
            bm.faces.new([bm.verts[i] for i in f_idx])

        bm.to_mesh(mesh)
        mesh.update()

        # add the mesh as an object into the scene with this utility module
        from bpy_extras import object_utils
        obj = object_utils.object_data_add(context, mesh, operator=self)

        return {'FINISHED'}


class AddSphereSegmentMLT(bpy.types.Operator):
    """Add a sphere segment mesh"""
    bl_idname = "mesh.mlt_segment_add"
    bl_label = "Sphere Segment MLAT/MLT"
    bl_options = {'REGISTER', 'UNDO'}

    lat0: bpy.props.FloatProperty(
        name="Lat0",
        description="Start latitude",
        min=-90, max=90.0,
        default=-90.0,
    )
    lat1: bpy.props.FloatProperty(
        name="Lat1",
        description="End latitude",
        min=-90, max=90.0,
        default=90.0,
    )
    lt0: bpy.props.FloatProperty(
        name="MLT/LST 0",
        description="Start MLT/LST",
        min=0, max=24.0,
        default=0.0,
    )
    lt1: bpy.props.FloatProperty(
        name="MLT/LST 1",
        description="End MLT/LST",
        min=0, max=24.0,
        default=24.0,
    )    
    height0: bpy.props.FloatProperty(
        name="Height0",
        description="Start height",
        min=0, max=10000,
        default=0,
    )
    height1: bpy.props.FloatProperty(
        name="Height1",
        description="End height",
        min=0, max=10000,
        default=500,    
    )
    numlons: bpy.props.IntProperty(
        name="NumLons",
        description="Number of longitude steps",
        min=3, max=255,
        default=64,
    )
    numlats: bpy.props.IntProperty(
        name="NumLats",
        description="Number of latitude steps",
        min=3, max=255,
        default=32,
    )   
    numheights: bpy.props.IntProperty(
        name="NumHeights",
        description="Number of height steps",
        min=3, max=255,
        default=3,
    )
    datetime: bpy.props.StringProperty(
        name="DateTime",
        description="UTC date/time for geomagnetic coordinate conversion",
        default="2020-05-17T00:00:00"
    )

    coordinate_options = (
        ('geographic', 'Geographic', 'Geographic coordinates'),
        ('quasi-dipole', 'Quasi-dipole', 'Quasi-dipole geomagnetic coordinates'),
        ('apex', 'Apex', 'Apex geomagnetic coordinates')
    )

    coordinatetype: bpy.props.EnumProperty(
        name="Coordinate type",
        items=coordinate_options,
        default="geographic",
    )

    # generic transform props
    align_items = (
        ('WORLD', "World", "Align the new object to the world"),
        ('VIEW', "View", "Align the new object to the view"),
        ('CURSOR', "3D Cursor", "Use the 3D cursor orientation for the new object")
    )
    align: bpy.props.EnumProperty(
        name="Align",
        items=align_items,
        default='WORLD',
        update=AddObjectHelper.align_update_callback,
    )
    location: bpy.props.FloatVectorProperty(
        name="Location",
        subtype='TRANSLATION',
    )
    rotation: bpy.props.FloatVectorProperty(
        name="Rotation",
        subtype='EULER',
    )

    def execute(self, context):

        t = timeseries.datetime_index[0].to_pydatetime()

        lon0, lon1 = lt_to_lon(self.lt0, self.lt1, self.coordinatetype, t)

        verts_loc, faces = sphere_segment(
            self.lat0, 
            self.lat1, 
            lon0, 
            lon1, 
            self.height0, 
            self.height1,
            self.numlats,
            self.numlons,
            self.numheights,
            self.coordinatetype,
            t
        )


        mesh = bpy.data.meshes.new("MLT sphere segment")

        bm = bmesh.new()

        for v_co in verts_loc:
            bm.verts.new(v_co)

        bm.verts.ensure_lookup_table()
        for f_idx in faces:
            bm.faces.new([bm.verts[i] for i in f_idx])

        bm.to_mesh(mesh)
        mesh.update()

        # add the mesh as an object into the scene with this utility module
        from bpy_extras import object_utils
        obj = object_utils.object_data_add(context, mesh, operator=self)

        # Store the inputs in custom object properties, so they can be used to create animation data later
        obj["lat0"] = self.lat0
        obj["lat1"] = self.lat1
        obj["lt0"] = self.lt0
        obj["lt1"] = self.lt1
        obj["height0"] = self.height0
        obj["height1"] = self.height1
        obj["numlons"] = self.numlons
        obj["numlats"] = self.numlats
        obj["numheights"] = self.numheights
        obj["datetime"] = self.datetime
        obj["coordinatetype"] = self.coordinatetype

        return {'FINISHED'}

def insert_keyframe(fcurves, frame, values):
    for fcu, val in zip(fcurves, values):
        fcu.keyframe_points.insert(frame, val, options={'FAST'})

class AnimateSphereSegmentMLT(bpy.types.Operator):
    """Add a sphere segment mesh"""
    bl_idname = "mesh.mlt_segment_animate"
    bl_label = "Add animation data"
    bl_options = {'REGISTER'}

    def execute(self, context):
        mesh = context.object.data
        action = bpy.data.actions.new("MeshAnimation")

        mesh.animation_data_create()
        mesh.animation_data.action = action

        data_path = "vertices[%d].co"

        # Set up f-curves for each vertex
        fcurves = {}
        for i, v in enumerate(mesh.vertices):
            fcurves[i] = [action.fcurves.new(data_path % v.index, index=i) for i in range(3)] # range(3) corresponds to x, y, z coordinates

        # Now loop over each frame and vertex and set keyframes
        for frame, time in enumerate(timeseries.datetime_index):
            t = pd.to_datetime(time).to_pydatetime()
            lon0, lon1 = lt_to_lon(context.object["lt0"], context.object["lt1"], context.object["coordinatetype"], t)
            newverts, faces = sphere_segment(
                context.object["lat0"], 
                context.object["lat1"], 
                lon0, 
                lon1, 
                context.object["height0"], 
                context.object["height1"],
                context.object["numlats"],
                context.object["numlons"],
                context.object["numheights"],
                context.object["coordinatetype"],
                t)
            for i, v in enumerate(mesh.vertices):
                co_kf = newverts[v.index]
                insert_keyframe(fcurves[i], frame, co_kf)

        return {'FINISHED'}


class VIEW3D_PT_spheresegment(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'Geospace'
    bl_label = 'Sphere segment'

    def draw(self, context):
        col = self.layout.column(align=True)
        if context.active_object is None:
            col.label(text="- No active object -")
        else:
            col = self.layout.column(align=True)
            col.prop(context.object, 'name')
            col.operator('mesh.mlt_segment_animate')

        if context.object:
            if not timeseries.datetime_index is None:
                if "coordinatetype" in context.object:
                    col.enabled = True
                else:
                    col.enabled = False
                    col.label(text="- Not an MLT slice object -")
            else:
                col.enabled = False
                col.label(text="- No valid timeseries -")
        else:
            col.enabled = False
            col.label(text="- No object selected -")
            

def menu_func_spheresegment(self, context):
    self.layout.operator(AddSphereSegment.bl_idname, icon='MESH_UVSPHERE')


def menu_func_mltspheresegment(self, context):
    self.layout.operator(AddSphereSegmentMLT.bl_idname, icon='MESH_UVSPHERE')


blender_classes = [
    VIEW3D_PT_spheresegment,
    AddSphereSegment,
    AddSphereSegmentMLT,
    AnimateSphereSegmentMLT,
]

def register():
    for blender_class in blender_classes:
        bpy.utils.register_class(blender_class)
    bpy.types.VIEW3D_MT_mesh_add.append(menu_func_spheresegment)
    bpy.types.VIEW3D_MT_mesh_add.append(menu_func_mltspheresegment)

def unregister():
    for blender_class in blender_classes:
        bpy.utils.unregister_class(blender_class) 
    bpy.types.VIEW3D_MT_mesh_add.remove(menu_func_spheresegment)
    bpy.types.VIEW3D_MT_mesh_add.remove(menu_func_mltspheresegment)

